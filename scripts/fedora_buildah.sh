set -xe

dnf update -y
dnf install arch-install-scripts btrfs-progs debootstrap dosfstools edk2-ovmf e2fsprogs squashfs-tools gnupg python3 tar veritysetup xfsprogs xz zypper systemd-container sbsigntools mkosi git podman buildah -y

dnf clean all

rm -rf /var/cache/yum
