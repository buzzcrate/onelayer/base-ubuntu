

set -xe

apt-get update -y
apt install curl software-properties-common -y
. /etc/os-release
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key | apt-key add -
apt-get update
apt-get -y upgrade
apt install podman buildah mkosi systemd-container multistrap bsdtar git -y
